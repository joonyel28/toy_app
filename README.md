# README
------------------------------------------------------------

Heroku App Link: https://agile-wildwood-79593.herokuapp.com/

BitBucket Link: https://bitbucket.org/joonyel28/toy_app/src/master/

------------------------------------------------------------
Book: https://www.railstutorial.org/book/beginning
Chapter 1-3

Create Toy App:

 Features

  - Create User

   Fields:

    - Name #required

    - Email # required

- Edit/Update User Details

- Display All Users

Create Repository in Bitbucket

Manage version control using Git

Deploy App in Heroku(Optional?)
